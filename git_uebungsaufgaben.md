## _git_ Übungsaufgaben

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110326723919640390</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/git_uebungsaugaben_</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Natürlich lernt man git am besten, in dem man es täglich nutzt. Wer aber die git-Integration in der IDE nutzt hat vielleicht keinen Bezug mehr zu den eigentlichen Konsolen-Befehlen des Versionscontrollsystems. Hier daher ein paar Fragen zu git, wie ich sie auch gerne in Klassenarbeiten stelle..._

### Leitfragen zum Versionscontrollsystem _git_

- Erläutere mithilfe einer Skizze den Unterschied zwischen den Befehlen `merge` und `rebase` im Versionskontrollsystem _git_.  <button onclick="toggleAnswer('git_lf1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf1">

Die Befehle `merge` und `rebase` führen beide unterschiedliche Entwicklungszweige (_Branches_) zusammen.

Bei einem _Merge_ bleiben beide _Branches_ erhalten und es entsteht ein neuer _Commit_, der die Änderungen zusammenführt:

![](images/graph-after-merge.png)

Bei einem _Rebase_ dagegen werden alle Änderungen des einen _Branches_ an den anderen angehängt. Im ursprünglichen _Branch_ werden diese Änderungen versteckt. Es entsteht somit ein linearer _git_-Graph ohne Verzweigungen. Im folgenden Bild sind die vorigen _Commits_ zur Verdeutlichung noch angezeigt (`hidden C`, `hidden B`), eigentlich sind sie nur noch im linearen _Branch_ vorhanden(`B'`, `C'`).

![Linearer Branch eines Rebases](images/graph-after-rebase.png)

_Infos dazu können [hier (link)](https://oer-informatik.de/git02-versionieren_mit_branches) nachgelesen werden._

</span>

- Welche Vorteile bietet `rebase` gegenüber `merge`?  <button onclick="toggleAnswer('git_lf2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf2">

Durch die lineare Struktur des _git_-Graphs erscheint das Projekt übersichtlicher. Im Log kann leichter nachvollzogen werden, welche Änderungen wann dazu gekommen sind, da es keine Verzweigungen gibt.

</span>


- Welche potenziellen Probleme können beim  _Rebasen_ auftreten, wenn auf ältere Versionen zurückgesprungen werden muss?  <button onclick="toggleAnswer('git_lf3')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf3">

Beim Rebase entstehen "virtuelle Commits", die es so nie in einer Entwicklungsumgebung gegeben hat, für die ggf. auch nie Tests gelaufen sind. Die Version `B'` im Graph unten hat es so nie gegeben, sie ist erst durch den _Rebase_ entstanden. 

![Linearer Branch eines Rebases](images/graph-after-rebase.png)

Faktisch wird dies selten zu Problemen führen, sollte aber im Auge behalten werden, wenn größere Projekte mit längeren Graphs per Rebase organisiert werden. 

Werden die Änderungen eines `main`-Branches an einen `feature`-Branch per `rebase` verschoben entstehen Probleme, wenn andere Entwickler*innen noch am vormals jüngsten `main`-Commit gearbeitet hatten - den gibt es dann ja nicht mehr.

</span>

- _git_ nutzt zur Identifizierung einzelner Versionen SHA1-Hashes, wohingegen andere VCS laufende Nummern (1, 2, 3) oder Revisionsnummern (1.1, 1.2 usw.) verwenden. Welche Vorteile und welche Nachteile ergeben sich aus der Identifizierung mit Hashes? <button onclick="toggleAnswer('git_lf4')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf4">

Hashes haben den Vorteil, dass keine zentrale Instanz benötigt wird, die die Reihenfolge der Zählung organisiert. Auf diesem Weg ist ein dezentrales VCS wie _git_ überhaupt erst möglich.

Hashes sind andererseits sehr unhandlich. Daher ermöglicht _git_, dass man mit abgekürzten Hashes arbeiten kann, um _Commits_ zu identifizieren.

</span>

- _git_ wird als dezentrales Versionskontrollsystem bezeichnet. Erläutere die Eigenschaft der Dezentralität von _git_ in Abgrenzung zu Versionskontrollsystemen mit zentralem Ansatz.  <button onclick="toggleAnswer('git_lf5')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf5">

Auch wenn häufig mit pseudo-zentralen Repositories bei Anbietern wie _gitlab_, _github_, _bitbucket_ usw. gearbeitet wird, ist jedes Repository eines _git_-Projekts gleichberechtigt und enthält alle Versionen der anderen Repository-Instanzen seit dem Stand der letzten Synchronisierung (pull/push). Somit kann von jedem lokalen Repository aus das Gesamtprojekt wieder hergestellt werden.

Zentrale Versionskontrollsysteme haben einen "single point of truth" (das zentrale Repository), der auch ein "single point of failure" werden kann, da nicht alle lokalen Clients über alle Informationen verfügen.

</span>


- Definiere, was im VCS git ein _**Repository**_ ist.  <button onclick="toggleAnswer('git_lf6')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf6">

Als _Repository_ wird in _git die Gesamtheit aller bisherigen Änderungen bezeichnet. Alle Versionsstände sind im _Repository_ gespeichert. Im Dateisystem befinden sich diese Daten im `.git`-Unterordner. Wenn man eine neue Version in das _Repostory_ übernimmt, spricht man von einem _Commit_.

</span>


- Definiere, was im VCS git ein _**Commit**_ ist.  <button onclick="toggleAnswer('git_lf7')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf7">

Ein _Commit_ ist ein Versionsstand, den man in ein _git_-Repository zur Versionierung übergibt. _Commits_ werden über Hashwerte eindeutig identifiziert und im _git_-Graph in ihrer Reihenfolge mit (ggf. mehreren) Vorgängern und Nachfolgern abgebildet.

</span>


- Definiere, was im VCS git ein _**Branch**_ ist.  <button onclick="toggleAnswer('git_lf8')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf8">

Ein _Branch_ ist ein Zeiger auf einen Commit. In der Regel zeigt ein Branch auf das Ende einer Verzweigung, so dass der Begriff häufig synonym mit "Verzweigung" gebraucht wird. Branches und Verzweigungen dienen dazu, den Workflow zu organisieren und parallel an unterschiedlichen Teilaufgaben zu arbeiten, ohne sich bereits in der Entwicklung mit Seiteneffekten der anderen Teilaufgaben beschäftigen zu müssen.

</span>


- In einem Satz: welche Aufgaben haben die _git_-Befehle add, commit, branch, checkout, rebase, merge, pull, fetch, push, clone  <button onclick="toggleAnswer('git_lf9')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf9">

`git add` : meldet Änderungen für den nächsten _commit_ an - hebt die Dateien vom _Workspace_ auf die _Stage_

`git commit` : Fügt einen neuen Versionsstand in das lokale _Repository_ ein.

`git branch` : Erstellt oder bearbeitet einen neuen Zeiger auf einen Commit, der für Verzweigungen genutzt wird.

`git checkout` : Ändert den Inhalt des _Workspaces_ auf einen anderen Versionsstand des _Repositories_.

`git rebase` : Fügt unterschiedliche _Branches_ zusammen, in dem es die Änderungen des einen an den anderen anhängt (lineare Struktur).

`git merge` : Fügt unterschiedliche _Branches_ zusammen, in dem es einen neuen _Commit_ erstellt, in dem die Änderungen beider _Branches_ enthalten sind.

`git fetch` : Holt die aktuellen Versionsstände eines _Remote-Repositorys_.

`git pull` : Holt die Änderungen (`fetch`) und führt sie direkt mit dem lokalen Anpassungen zusammen (`merge`, `rebase`).

`git push` : Überträgt die lokalen Änderungen in das _Remote-Repository_.

`git clone` : Erstellt ein lokales _Repository_ auf Basis eines _Remote-Repository_.

</span>


- Commits werden in _git_ mit einer Nachricht/Beschreibung versehen. Nenne einfache Konventionen, wie diese Nachricht häufig aufgebaut ist sowie ein Beispiel für eine solche Nachricht. <button onclick="toggleAnswer('git_lf10')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_lf10">

Es gibt eine Vielzahl von Konventionen, wie _Commit_-Nachrichten aufgebaut sein sollen. Häufig wird in der ersten Zeile der folgende Aufbau genutzt:

`TYPE[(SCOPE)]: SUBJECT`

Wobei `TYPE` beschreibt, um welche Art Anpassung es sich handelt (z.B. `fix`, `refactor` oder `feature`),
`[Scope]` das betroffene Modul benennt, an dem es zu Änderungen gekommen ist
`SUBJECT` die Änderung selbst beschreibt, häufig im Präsens gehalten, da es sich im _git_-Graph dann besser liest.

Im Idealfall folgt danach ein [BODY] und ein [FOOTER], die die Änderungen etwas genauter beschreiben.

</span>


### Git Szenario 1

Du befindest dich in der Konsole im Ordner `meinProjekt`, in dem lediglich eine Datei liegt (`ReadMe.md`). 

![Ein Verzeichnis im Windows-Explorer mit der Datei Readme.md](images/workspace_uebung.png)

a) Nenne die vollständigen _git_-Befehle, mit denen Du ein neues _git_-Repository erzeugen und die Datei `ReadMe.md` dem Hauptzweig des Repositorys (also `main`) mit der Nachricht "initial commit" hinzufügst! <button onclick="toggleAnswer('git_sz1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_sz1">

```bash
git init [-b main]
git add ReadMe.md
git commit -m "initial commit"
```

</span>


b) `Readme.md` wurde im Editor geändert. Nenne die vollständigen Befehle, um die Änderung mit dem Betreff "add description" ins Repository einzufügen (wir bleiben im `main`-Branch). 
 <button onclick="toggleAnswer('git_sz2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_sz2">

```bash
git add ReadMe.md
git commit -m "add description"
```

</span>

c) Eine neue Datei, `index.html` wurde erstellt. Nenne die vollständigen Befehle, um die Datei in einem Branch `feature/website` mit dem Betreff "feat add landing page" ins Repository einzufügen.  
 <button onclick="toggleAnswer('git_sz3')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_sz3">

```bash
git branch feature/website
git checkout feature/website
oder einfach: git checkout -b feature/website
git add index.html
git commit -m "feat add landing page"
```

</span>


d) Wir befinden uns im `main`-Branch (siehe _git_-Graph in Aufgabe e). Es sollen die beiden Branches (`main`, `feature/website`) _gemerged_ werden. Der neue Commit soll im `main`-Branch liegen und der Head darauf zeigen. Die Nachricht soll "merge feature website" lauten. Nenne die vollständigen dafür nötigen _git_-Befehle! 
 <button onclick="toggleAnswer('git_sz4')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_sz4">

**Variante 1:**

```bash
git merge feature/website  -m "merge feature website"
```

**Variante 2**
Von der anderen Seite sind mehrere Befehle notwendig:

```bash
git checkout feature/website
git merge main -m "merge feature website"
```

Damit ist zwar die Struktur richtig, aber `HEAD` zeigt noch auf den falschen Branch:

```bash
$ git log --all --decorate --oneline  --graph
*   7d2afe5 (HEAD -> feature/website) merge feature website
|\
| * 1eebaeb (main) fix typos
* | b4548d0 feat add landing page
|/
* a664760 add description
* c0d1470 initial commit
```

Wir müssen das also noch ändern:

```bash
git checkout main
git merge feature/website
```

Jetzt sollte alles passen:

```bash
$ git log --all --decorate --oneline  --graph
*   7d2afe5 (HEAD -> main, feature/website) merge feature website
|\
| * 1eebaeb fix typos
* | b4548d0 feat add landing page
|/
* a664760 add description
* c0d1470 initial commit
```

</span>


e) Vervollständige den _git_-Graph so, wie er nach dem _Mergen_ (Aufgabe d) aussieht. Nenne zur Identifizierung die Commit-Nachricht (kann abgekürzt sein). Streiche Commits durch, die nicht mehr existieren. Trage für alle Branches sowie für Head ein, wo diese sich aktuell befinden (streiche ggf. vorhandene Einträge). Der neuste Commit ist oben. Hashes müssen nicht genannt werden, nur Commit-Nachrichten.

![Git-graph, 3 commits im main, nach dem zweiten ein feature-Branch mit einem commit, HEAD auf main](images/graph_uebung.png)

<button onclick="toggleAnswer('git_sz5')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_sz5">

![Gleicher Graph wie oben, nur neuer merge-Commit, HEAD zeigt auf main](images/graph_uebung_loesung.png)

```bash
$ git log --all --decorate --oneline  --graph
*   827fe07 (HEAD -> main) merge feature website
|\
| * b4548d0 (feature/website) feat add landing page
* | 1eebaeb fix typos
|/
* a664760 add description
* c0d1470 initial commit
```

</span>

f) Merge-Konflikt: Es erscheint beim Versuch zu mergen eine Fehlermeldung wegen konkurrierenden Änderungen. Welche Schritte sind erforderlich? (Exakte Befehle sind nicht erforderlich, nenne einfach, was zu tun ist.) <button onclick="toggleAnswer('git_sz6')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_sz6">

1. Der Konflikt muss behoben werden, bevor weitergearbeitet werden kann. 
2. Die Dateien, bei denen es zu Konflikten kam, müssen per `git add` hinzugefügt werden 
3. Es muss per `git commit` der merge-commit erzeugt werden.

</span>


g) Wir befinden uns wieder im main-Branch an der Stelle vor Aufgabe d (siehe _git_-Graph in Aufgabe h), gehen also eine zweite Variante mit der gleichen Ausgangssituation durch. Diesmal sollen die beiden Branches (`main`, `feature/website`) _gerebased_ werden. Der neue Commit soll im `main`-Branch liegen und der `HEAD` darauf zeigen. Die Nachricht soll "integrate feature website" lauten. Nenne die vollständigen dafür nötigen _git_-Befehle!  <button onclick="toggleAnswer('git_sz6')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_sz6">

```bash
git checkout feature/website
git rebase main
git checkout main
git merge feature/website
git add . && git commit -m „integrate feature website”
```

</span>

h) Vervollständige den _git_-Graph so, wie er nach dem _Rebase_ (Aufgabe g) aussieht. Nenne zur Identifizierung die _Commit_-Nachricht (kann abgekürzt sein). Streiche _Commits_ durch, die nicht mehr existieren. Trage für alle _Branches_ sowie für `HEAD` ein, wo diese sich aktuell befinden (streiche ggf. vorhandene Einträge). Der neuste Commit ist oben. Hashes müssen nicht genannt werden, nur _Commit_-Nachrichten.

![Git-graph, 3 commits im main, nach dem zweiten ein feature-Branch mit einem commit, HEAD auf main](images/graph_uebung.png)

<button onclick="toggleAnswer('git_sz7')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="git_sz7">

![Git-graph wie oben, allerdings gibt es oberhalb der 3 Commits im main-Branch zwei weitere neue (per rebase)](images/graph_uebung_loesung_rebase.png)
 
</span>

### _git_ Beispielszenario 2

Gegeben ist ein Repository mit einem initialen Commit und einem zweiten, mit dem Code veröffentlicht wurde (Branch `main`). 

- Es soll ein neuer Branch `killerFeature` erstellt werden,
- dieser soll ausgecheckt werden,
- per `touch datei.txt` simulieren wir Veränderungen an der Codebasis von `killerFeature`,
- diese Veränderungen sollen in das Repository übernommen werden,
- der Branch `main` soll in den Workspace geladen werden,
- es wird eine Datei per `echo “Hallo Welt”>>beispiel.txt` erzeugt,
- diese Datei wird dem `main`-Branch hinzugefügt,
- `killerFeature` wird in den Workspace geladen und auf den `main` rebased

Nenne die dazu nötigen vollständigen git-Befehle in der Reihenfolge, in der sie ausgeführt werden müssen.

## _git_ Beispielszenario 3

Gegeben ist ein remote Repository mit einem initialen Commit und einem zweiten, mit dem Code veröffentlicht wurde (befindet sich auf dem entfernten Repository im Branch `main`). 

- Klone das remote-Repository!
- Nach dem Klonen wird im ursprünglichen remote-Repository ein Commit (durch eine*n andere*n Entwickler*in) durchgeführt.
- Auch unsere Codebasis ändert sich (z.B. vereinfacht per `echo “Hallo Welt”>>beispiel.txt`)
- Die neue Datei wird dem lokalen `main`-Branch hinzugefügt
- Die Änderungen sollen in zwei unterschiedlichen Varianten zusammengefügt werden:
a) per merge
b) per rebase

Die geänderten Daten sollen auf das remote-Repository übertragen werden.

Nenne die dazu nötigen vollständigen _git_-Befehle in der Reihenfolge, in der sie ausgeführt werden müssen. Nenne auch für die Varianten a) und b) jeweils den an dieser Stelle nötigen Befehl.


### Teile dieses Tutorials

* [Ein git-Repository anlegen und lokal commiten](https://oer-informatik.de/git01-repo_anlegen_und_lokal_commiten)

* [Versionieren mit Branches](https://oer-informatik.de/git02-versionieren_mit_branches)

* [Git Remote nutzen](https://oer-informatik.de/git03-remote_nutzen)

* [Übungsaufgaben zu _git_](https://oer-informatik.de/git_uebungsaufgaben)

* [Readme.md Dateien für _git_-Repositorys erstellen](https://oer-informatik.de/git04-readme-erstellen)

### Weitere Übungsaufgaben zur Versionsverwaltung `git`

- Zentrale Seite für Übungen zum Branching: [Learn git branching](https://learngitbranching.js.org/?locale=de_DE): animiertes Tutorial v.a. gut, um die Navigation im git-Tree zu verstehen.