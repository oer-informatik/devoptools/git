## _git_ `Readme.md`-Dateien erstellen

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110440566399038321</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/git04-readme-erstellen</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Gute Software braucht gute Dokumentation - ohne unnötigen Aufwand. Eine erste Anlaufstelle ist häufig die `Readme.md`-Datei in git-Repositorys. Aber: für wen schreibe ich die eigentlich? Was sollte, was muss da rein? Wie formatiere ich die `Readme.md`-Datei mit Markdown?_

### First things first: Für wen schreibe ich die `Readme.md` und was will ich ihr/ihm sagen?

Was für diesen Text gilt, gilt auch für jede `Readme.md`: Wir müssen uns zunächst im Klaren sein, wer unsere Adressaten sind und welche Nachricht wir ihnen übermitteln wollen. Ein paar Beispiele:

- Das Projekt vereinfacht einen Workflow, den ich alle paar Monate habe. Es ist sehr unwahrscheinlich, dass jemand anders ein ähnliches Problem hat und das Projekt nutzen wird. Die Nachricht für _mein zukünftiges ich_ wäre also etwa "Wie wende ich die Software an?", "Wo habe ich nochmal die Konfigurations-Flags versteckt?". Für alle anderen fasse ich kurz zusammen, welches Problem das Projekt löst, damit sie wissen, ob dieses Projekt für sie relevant ist.

- Ich habe eine sehr spezielle Library erstellt, die nur in unserem Unternehmen genutzt wird (und auch gar nicht öffentlich verteilt wird). Der Adressatenkreis grenzt sich damit auf  _mein zukünftiges ich_ und Kolleg*innen ein, denn auch die `Readme.md` wird das Intranet nie verlassen. Als _User_ und _Developer_ wollen die Adressaten zusätzlich zu dem oben genannten wissen, wie sich das Projekt in die Unternehmensumgebung integriert, wo die Schwachstellen und aktuellen ToDos sind.

- Ich habe Plugin für einen weit verbreiteten Editor erstellt, den viele nutzen. Mein Adressat wären also Anwender, die wissen wollen, was dieses Plugin an, wie ich es installiere und benutze. Vielleicht ist meine Intention auch, Mitentwickler*innen zu finden - dann sollte die `Readme.md` auch diesen Fokus setzen. In jedem Fall wäre die `Readme.md` hier ein Aushängeschild und sollte auch die nötige Aufmerksamkeit bei der Erstellung genießen. 

- Ich habe ein Projekt für die Berufsschule / die Uni erstellt. Das eigentliche Projektziel ist, zu zeigen, dass ich bestimmte Fertigkeiten anwenden kann. Adressaten wären v.a. die Bewertenden, meine _Peergroup_ oder andere, die ähnliche Projekte umsetzen. Im Fokus sollte also auch die neu erlernte Fertigkeit stehen.

Ich denke, es ist klar geworden: ein _OpenSource_-Projekt benötigt eine andere `Readme.md` als ein firmeninternes Projekt usw. Bei allem folgenden sollte also stets der Adressatenkreis im Blick bleiben: brauche ich diesen Punkt in meinem Fall wirklich?

### Qualitätsmerkmale von Softwaredokumentation

Häufig ist die `Readme.md`-Datei neben Testfällen, aussagekräftigen Methoden- und Variablennamen und Inline-Kommentaren die einzige Dokumentation, die zu einer Software besteht. Sie muss daher von Anfang an in der Qualitätssicherung mitgedacht werden - und bei jeder Veränderung der Software mit qualitätsgesichert werden.

Die beiden IT-Sicherheitsexpert*innen Caroline Krohn und Manuel Atug stellen in diesem Zusammenhang einen Bezug zwischen IT-Dokumentationen und Nachhaltigkeit her, der die Verantwortung für IT-Sicherheit, sicheren Betrieb auch für "zukünftige Generationen" (also auch kurzfrisige Nachfolger) und Transparenz mit der Dokumentation herausstellt.^[siehe iX 06/2023 S.36 ["Dokumentation und Nachhaltigkeit" (hinter Bezahlschranke)](https://www.heise.de/select/ix/2023/6/2305212363315891171)]. 

Die von ihnen genannten Qualitätskriterien, die insbesondere für die Dokumentation von freie Software gelten, sind: 

- Vollständigkeit, 

- Übersichtlichkeit, 

- Nachvollziehbarkeit, 

- Verständlichkeit, 

- Strukturiertheit, 

- Korrektheit, 

- Objektivität und 

- Editierbarkeit.

Auch diese Qualitätsmerkmale müssen natürlich auf das jeweilige Projekt zugeschnitten werden. Als Ausgangspunkt, von dem aus man begründet abweichen kann, halte ich diese Auflistung aber für sehr geeignet.

### Inhalt einer Readme: Was sind denkbare Bestandteile?

Ich sehe gerne auf den ersten Blick im Repository, ob das jeweilige Projekt für mich überhaupt relevant ist. Daher wünsche ich mir häufig, dass ich auf den ersten Blick die Frage beantworten kann:

**Was ist das für ein Projekt - und wann darf ich es benutzen / kopieren / weiterverarbeiten?**

- Projekttitel

- Kurze Beschreibung des Projekts/Repos als _oneliner_ (nach der Lizenz nochmal etwas ausführlicher)

- Welche Lizenz nutzt das Projekt?

Damit wären in den ersten drei Zeilen die wesentlichen Informationen gegeben. Sollte das Projekt länger werden, wäre jetzt ein guter Platz für ein Inhaltsverzeichnis. Denn ab hier kann man mit selbst gewähltem Detaillierungsgrad sehr ausführlich werden.

**Welches Problem löst das Projekt?**

- Welche Ausgangslage gab es und wo lag das Problem, dass durch das Projekt gelöst wird?

- Für welche Einsatzgebiete ist dieses Projekt gedacht?

Gerade bei öffentlichem Sourcecode ist dieser Absatz wichtig.

**Wie installiere ich das Projekt**

- Woher bekomme ich den Quellcode / die Binaries (Installationsdateien)?

- Wie wird das Projekt installiert? Gibt es Screencasts oder Schritt-für-Schritt Anleitungen des Installationsprozesses?

- Welche Voraussetzungen und Abhängigkeiten hat das Projekt, welche Ressourcen werden genutzt und benötigt?

**Wie nutze ich das Projekt**

- Wie wird es gestartet?

- Wie führe ich die Standard-Anwendungsfälle durch (ggf. Screenshots, Screencasts)?

- Auf welchen Benutzertests beruht die Abnahme? Damit sind i.d.R. die häufigsten Anwendungsfälle dokumentiert.

- Bei größeren Projekten muss man sicher auf externe Quellen verweisen: Wo finde ich die Anwender-Dokumentation?

**Wer hat alles zum Projekt beigetragen?**

Häufig basieren Projekte auf Vorgängerprojekten oder haben Anleitungen genutzt. Die Quellen zu nennen (und zu verlinken) hilft allen, die Informationen zu bestimmten Problemen suchen. Und es ist ein Dank an die Projekte, die uns selbst geholfen haben.

Falls an diesem Projekt selbst mehrere Entwickler*innen mitgearbeitet haben, kann es sinnvoll sein, hier eine Liste mit Kontaktdaten zu nennen - oder ggf. auf die Changelog-Datei zu verweisen, aus der hervorgeht, wer alles beigetragen hat.

**Was kann ich tun, wenn ich Fragen zum Projekt habe?**

- Welche bekannten Bugs/Kompatibilitätsprobleme gibt es?

- Wo kann ich Fragen an die Entwicklerin stellen (Kontakt Autoren)?

- Wo kann ich Bugreports einsehen und durchsuchen?

- Wo kann ich Featurerequests erstellen?

- Welche Features sind noch geplant?

**Wie kann ich das Projekt parametrisieren, verändern, weiterentwickeln?**

- Welchen Aufbau, welche Struktur hat das Projekt? Hier helfen Diagramme und Visualisierungen!

- Welche öffentlichen Methoden sollte man kennen?

- Wo finde ich eine Entwicklerdokumentation?

- Wo finde ich den Quelltext?

- Wie starte ich die Testsuite?

- Welche Entscheidungen wurden im Projektverlauf getroffen, die für andere in ähnlicher Situation interessant sein können?

- Warum wurden andere alternative Lösungswege/Techniken nicht genutzt?

- Welche Probleme/offene Fragen gibt es momentan bei der Umsetzung/Entwicklung?

### Kleines Markdown HowTo für die `Readme.md`

Die `Readme.md`-Dateien in _git_-Repositories werden mithilfe von Markdown formatiert. Markdown verfügt über eine zurückhaltende Auszeichnung (Formatierungsbefehle). Dadurch bleiben die Quelltexte leicht lesbar und in jedem Editor bearbeitbar. 

Ein guter Start zum Ausprobieren von Markdown ist beispielsweise der Online-Editor [CodiMD (hier als HackMD)](hackmd.okfn.de/). Die wichtigsten Auszeichnungselemente von Markdown sind folgende:

- Absätze werden immer durch eine Leerzeile gekennzeichnet. 

    ```markdown
    Dieser Text
    wird in eine Zeile
    geschrieben.
    
    Dieser nicht.
    ```

    Dieser Text
    wird in eine Zeile
    geschrieben.
    
    Dieser nicht.
    
- Überschriften werden mit vorangestelltem `#` notiert (Leerzeilen davor und danach!):

    ```markdown
    # Überschrift

    ## Unterüberschrift

    ### Unterunterüberschrift...
    ```

- Ich kann _kursiv_, **fett** oder **_fett und kursiv_** schreiben.

    ```markdown
    Ich kann _kursiv_, **fett** oder **_fett und kursiv_** schreiben.
    ```

- Links werden wie folgt notiert:

    ```markdown
    [Artikel über Readme.md-Dateien](https://oer-informatik.de/git04-readme-erstellen)
    ```

    [Artikel über Readme.md-Dateien](https://oer-informatik.de/git04-readme-erstellen)

- Bilder sollten immer auch einen ALT-Text enthalten:

    ```markdown
    ![ein Bild mit normaler Bildunterschrift](https://oer-informatik.gitlab.io/db-sql/dml-select-queries/image/select-railroad-projektion-selektion.png)
    ```

    ![ein Bild mit normaler Bildunterschrift](https://oer-informatik.gitlab.io/db-sql/dml-select-queries/image/select-railroad-projektion-selektion.png)

- Tabellen lassen sich am einfachsten so darstellen:

    ```markdown
    | Hier | kommen | die |Überschriften<br/> hin|
    |--------:|:-------|:--------:|---------|
    | Rechts | Links | Mittig |Default |
    |Code geht `auch` |Oder Formeln $E = m \cdot c^2$ |Bei mehrzeiligem<br/>Text muss man<br/>mit HTML<br/>schummeln||
    | es kann | auch etwas leer |  |sein |
    Table: Eine Linientabelle
    ```

    | Hier | kommen | die |Überschriften<br/> hin|
    |--------:|:-------|:--------:|---------|
    | Rechts | Links | Mittig |Default |
    |Code geht `auch` |Oder Formeln $E = m \cdot c^2$ |Bei mehrzeiligem<br/>Text muss man<br/>mit HTML<br/>schummeln||
    | es kann | auch etwas leer |  |sein |
    Table: Eine Linientabelle 


- Aufzählungen können auf unterschiedliche weise realisiert werden:

    ```markdown
    - Erster Gedanke

    - Nächster Gedanke

    - Letzter Gedanke

    1. Nummerierter Gedanke

    2. Nächste Nummer

    2. Trotz falscher Zahl korrekt in der Ausgabe

    * Ohne Nummerierung Variante 2

    * Wie bei Bindestrich
    ```
    - Erster Gedanke

    - Nächster Gedanke

    - Letzter Gedanke

    1. Nummerierter Gedanke

    2. Nächste Nummer

    2. Trotz falscher Zahl korrekt in der Ausgabe

    * Ohne Nummerierung Variante 2

    * Wie bei Bindestrich

- Fußnoten werden so umgesetzt.

    ```markdown
    Steile These^[Quelle der steilen These]
    
    Steile These aus dem Internet^[Quelle der steilen These [mit link](http://die.wirklich-ganze-wahrheit.de)]  
    ```

    Steile These^[Quelle der steilen These]
    
    Steile These aus dem Internet^[Quelle der steilen These [mit link](http://die.wirklich-ganze-wahrheit.de)] 

- Codeschnipsel haben das folgende Format (Leerzeile vor und hinter dem Codeblock setzen!):

    ````markdown
    ```python
    def lagest_signed_int(byte_count):
        largest = 2**(byte_count-1)-1
        return largest
    ```

    ```java
    package de.csbme.ifaxx
    
    public class Konto{
        private Double kontostand;

        public Double getKontostand(){
            return kontostand;
    }   }
    ```
    

    ```sql
    SELECT * FROM db.table WHERE text LIKE '%MSSQL%'
    ```
    ````

    ```python
    def lagest_signed_int(byte_count):
        largest = 2**(byte_count-1)-1
        return largest
    ```

    ```java
    package de.csbme.ifaxx
    
    public class Konto{
        private Double kontostand;

        public Double getKontostand(){
            return kontostand;
    }   }
    ```
    

    ```sql
    SELECT * FROM db.table WHERE text LIKE '%MSSQL%'
    ```
    
- In einigen Webfrontends von _git_-Diensten (github, gitlab, bitbucket) werden PlantUML-Scripte direkt als Grafik interpretiert:

    ````markdown
    ```plantuml
    @startuml
    class Rocket{
    -width:int
    -height:int
    +move()
        +draw(screen: Screen)
    }
    @enduml
    ```
    ````

- Mathematische Formeln können mit der LaTeX-Notation in Dollarzeichen eingefasst werden. Die Notation ist etwas Übungssache, aber Seiten wie [der Editor von zahlen-kern.de](https://www.zahlen-kern.de/editor/) sind eine gute Einstiegshilfe.

    ```markdown
    $$W_{primär} = \frac{\eta}{W_g}$$
    ```

    $$W_{primär} = \frac{\eta}{W_g}$$

Wenn die so erstellte Markdown-Datei dann noch `Readme.md` heißt, und sich im Wurzelverzeichnis des Projektrepositorys befindet, dann wird sie auf der Website des Repositorys bei gitlab, github, bitbucket und Co. direkt geöffnet.

### Fazit

Es gibt keinen _einzig richtigen_ Weg ein `Readme.md` zu gestalten. Der erste und wichtigste Schritt ist es, sich Gedanken darüber zu machen, wem man mit dem Text wobei helfen will und darauf basierend Inhalt und Umfang anzupassen. Wenn dann die Datei noch mit in die Qualitätssicherung mit aufgenommen wird (auch bei Erweiterungen), der ist auf einem guten Weg. 

Die oben genannten Aspekte können sicher dabei unterstützen, eine passend zugeschnittene `Readme.md` zu erstellen. Die eigentliche Arbeit ist aber, die passende Form, den passenden Inhalt und den passenden Umfang individuell zu definieren.

### Teile dieses Tutorials

* [Ein git-Repository anlegen und lokal commiten](https://oer-informatik.de/git01-repo_anlegen_und_lokal_commiten)

* [Versionieren mit Branches](https://oer-informatik.de/git02-versionieren_mit_branches)

* [Git Remote nutzen](https://oer-informatik.de/git03-remote_nutzen)

* [Übungsaufgaben zu _git_](https://oer-informatik.de/git_uebungsaufgaben)

