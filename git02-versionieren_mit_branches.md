## Versionieren mit unterschiedlichen Zweigen (Branches)

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110326723919640390</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/git02-versionieren_mit_branches</span>

> **tl/dr;** _(ca. 30 min Lesezeit): Ein großer Vorteil von git ist, dass wir zeitgleich an unterschiedlichen Komponenten eines Projekts arbeiten können, und diese Entwicklungen zu definierten Zeitpunkten wieder zusammenführen können. Diese Entwicklung in Branches minimiert Seiteneffekte, fordert aber etwas Grips... Neben diesem Teil gibt es noch die beiden Tutorialfolgen [Ein git-Repository anlegen und lokal commiten](https://oer-informatik.de/git01-repo_anlegen_und_lokal_commiten) und [Git Remote nutzen](https://oer-informatik.de/git03-remote_nutzen)._

### Teile und herrsche: Entwickeln in Zweigen

Bislang haben wir immer im selben Zweig (main / früher oft: master) unserer Entwicklung gearbeitet.

```bash
$ git status
```

```
On branch main [...]
```

Das ist jedoch nicht praktikabel, wenn wir zeitgleich an unterschiedlichen Features arbeiten (oder unterschiedliche Entwickler am gleichen Projekt) und wir beständig mit den Seiteneffekten der anderen Entwicklungen kämpfen müssen. Daher ist es sinnvoll, zu definierten Zeitpunkten Schnittstellen zu schaffen - und zwischen diesen Schnittstellen relativ frei von Abhängigkeiten zu anderen Features/Projektbeteiligten arbeiten zu können.

### Einen neuen Zweig/Branch anlegen

Man zweigt also mit git an einem bestimmten Punkt die Entwicklung ab und erstellt einen neuen Zweig (Branch) mit dem Befehl `git branch branchname`. In diesem Branch können wir eine neue Funktionalität entwickeln und diese erst wenn sie fertiggestellt ist wieder in das Hauptprojket integrieren. Wir wollen die Begrüßung der `hallo.py` als Feature in eine Python-Funktion auslagern.

```bash
$ git branch feature/Begruessung
```

Allerdings haben wir den Branch bislang nur erzeugt, als aktiven Branch müssen wir ihn noch auswählen (wie sich schnell mit `git status` überprüfen lässt).

```bash
$ git checkout feature/Begruessung
```

```
Switched to branch 'feature/Begruessung'
```

Abkürzung: einen Branch erzeugen und direkt auswählen kann man mit `git checkout -b branchName`

Als neues Feature wird jetzt die Methode implementiert:

```python
def begruessung():
    name = input("Bitte geben Sie den Namen ein: ")
    print("Hallo", name, "!")

begruessung()
print("Auf Wiedersehen!")
```

... und die Änderungen ins Repository übernommen:


```bash
$ git add -A; git commit -m "refactor: extract func begruessung()"
```

Gut. Aber der Name sollte als Parameter an die neue Methode übergeben werden, damit er auch zur Verabschiedung genutzt werden kann.

```python
def begruessung(name:str):
    print("Hallo", name, "!")

name = input("Bitte geben Sie den Namen ein: ")
begruessung()
print("Auf Wiedersehen!")
```

O.k. So könnte es klappen. Die neuste Anpassung muss noch ins Repository übernommen werden:

```bash
$ git add -A; git commit -m "refactor: add param name for func begruessung()"
```

Gut. Wenn das geklappt hat, sollten wir im Log die beiden neuen Commits als Bestandteil des Zweigs `feature/Begruessung` sehen:

```bash
$ git log --oneline
```

```
46a682f (HEAD -> feature/Begruessung) refactor: add param name for func begruessung()
fb45c39 refactor: extract func begruessung()
896a1da (main) docs: add readme
7a910bf new feature: say Good-bye
d04664e new feature: say 'Hello NAME'
```

Das scheint geklappt zu haben: `HEAD` zeigt auf den neusten Commit (der dem Zweig `feature/Begruessung` zugeordnet ist). der `main`-Zweig endet momentan bei Commit `896a`.

### Ein zweites Feature anlegen

Parallel zur Entwicklung der Begrüßung soll auch die Verabschiedung angepasst werden. Ausgangspunkt ist auch hier der `main`-Zweig. Zunächst müssen wir diesen Zweig wieder auswählen:

```bash
$ git checkout main
```

```
Switched to branch 'main'
```

Aber: Hoppla- was ist jetzt passiert? Die Änderungen mit der neuen Funktion sind auf einmal weg?!? Offensichtlich passt _git_ beim Checkout auch den Workspace an.

Wir erstellen zunächst einen neuen Branch und wählen ihn auch direkt als aktiven Commit aus (Kurzschreibweise):

```bash
$ git checkout -b feature/Verabschiedung
```

Jetzt müssen wir noch das neue Feature implementieren: eine Funktion zur Verabschiedung in der `hallo.py`:

```python
name = input("Bitte geben Sie den Namen ein: ")
print("Hallo", name, "!")
tschuess(name)
def tschuess(name:str):
    print("Auf Wiedersehen,", name, "!")
```

Die Änderungen müssen noch ins Repository übertragen werden:

```bash
$ git add -A; git commit -m "refactor: extract func tschuess()"   
```

### Zwei Branches mergen

Wir haben jetzt im Ganzen drei Branches: zwei neue Featurebranches an den "Enden" des _git_-Baums und der `main`-Branch, der an der Verzweigung steht, aktiv ausgewählt ist derzeit `feature/Verabschiedung` (`HEAD`):

![Der _git_-Baum mit den beiden Feature-Branches und main an der Verzweigung](images/graph-two-branches.jpg)

Etwas weniger plastisch lässt sich dieser _git_-Baum auch mit `git log` anzeigen.

```bash
$ git log --all --decorate --oneline --graph
```

`feature/Verabschiedung` ist aktuell ausgewählt (`HEAD`), feature/Begruessung steht bei `46a6` und `main` noch bei `896a`.

```
* 529594d (HEAD -> feature/Verabschiedung) refactor: extract func tschuess()
| * 46a682f (feature/Begruessung) refactor: add param name for func begruessung()
| * fb45c39 refactor: extract func begruessung()
|/
* 896a1da (main) docs: add readme
* 7a910bf new feature: say Good-bye
* d04664e new feature: say 'Hello NAME'
```

Wir wollen jetzt zunächst `feature/Verabschiedung` und `main` zusammenführen. Dazu wechseln wir nach `main` und _mergen_ mit dem Branch `feature/Verabschiedung`

```bash
$ git checkout main
$ git merge feature/Verabschiedung
```

Da `main` keine weiteren Änderungen enthält, werden einfach die Änderungen des Feature-Branches übernommen. Im Graphen kann man erkennen, dass `main` jetzt nicht mehr an der Abzweigung steht, sondern am Ende des Zweiges:

![Merge aus main und feature/Verabschiedung - main steht jetzt am Ende des Zweigs](images/graph-first-merge-main.png)

> Beim Mergen werden die Änderungen des als Argument genannten Branches (im Beispiel: `feature/Verabschiedung`) in den aktuell ausgewählten Branch (im Beispiel `main`) übernommen. Wir bleiben dabei im aktuellen Branch (`main`), der gemergte Branch zeigt weiterhin auf denselben letzten Commit dieses Branches wie vor dem Mergen.
> 

### Merge-Konflikte lösen

Im nächsten Schritt _mergen_ wir jetzt das zweite Feature. Dazu müssen wir `main` ausgecheckt haben (ist momentan der Fall, andernfalls: `git checkout main`). Danach wird analog zu oben der Branch `feature/Begruessung` mit dem aktuell ausgewählten (`main`) vereint:

```bash
$ git merge feature/Begruessung
```

Das geht nicht so reibungslos. _git_ kann nicht eindeutig ermitteln, welche der neuen Varianten behalten werden soll:

```
Auto-merging hallo.py
CONFLICT (content): Merge conflict in hallo.py
Automatic merge failed; fix conflicts and then commit the result.
```

Das passiert häufig dann, wenn Änderungen in unterschiedlichen Zweigen die selbe Zeile betreffen, oder wenn die Änderungen bezogen auf den Gesamtumfang zu groß sind, um die Position für die Einfügung zu bestimmen. Wir müssen also händisch ran.

_Git_ befindet sich jetzt im merge-Modus - zu erkennen u.a. an der Eingabeaufforderungsmeldung (|merge). Im merge-Modus sind alle Dateien noch _gestaged_, die sich problemlos _mergen_ lassen - für alle anderen sind drei Schritte erforderlich:

1.	Der Konflikt muss behoben werden, bevor weitergearbeitet werden kann.

2.	Die Dateien, bei denen es zu Konflikten kam, müssen per `git add` hinzugefügt werden.

3.	Es muss per `git commit` der merge-commit erzeugt werden.

Dazu öffnen wir die Datei, die Konflikte verursacht hat, im Editor:


```python
def begruessung(name:str):
    print("Hallo", name, "!")

name = input("Bitte geben Sie den Namen ein: ")
<<<<<<< HEAD
print("Hallo", name, "!")
tschuess(name)
def tschuess(name:str):
    print("Auf Wiedersehen,", name, "!")
=======
begruessung()
print("Auf Wiedersehen!")
>>>>>>> feature/Begruessung
```

An allen Stellen, die _git_ nicht selbst lösen konnte, werden die beiden Varianten zwischen spitzen Klammern (`<<<`, `>>>`) getrennt durch Gleichheitszeichen (`===`) nach dem folgenden Muster dargestellt:

```diff
<<<<<<< HEAD
hier ist der Text im aktuellen Branch
=======
hier ist der Text im Branch, der gemergt werden soll
>>>>>>> NameDesZuMergendenBranches
```

Nachdem im Editor der problematische Bereich angepasst wurde, muss die geänderte Datei noch auf die _Stage_, um dann ins Repository übernommen zu werden.

```bash
$ git add -A; git commit -m "feat: integrate two features"
```

Im neuen Graphen ist der `HEAD` (der aktuelle Commit) auf dem Merge-Commit des `main`-Branches, die anderen Branches enden jeweils mit dem letzten Commit vor der Zusammenführung:

![Merge aus main und beiden Features - main steht hinter der Vereinigung der Branches](images/graph-after-merge.png)

Neben dieser manuellen Konfliktlösung kann auch ein _merge-tool_ genutzt werden, dazu sind folgende Schritte erforderlich:

*	Ein Merge-Tool muss herausgesucht und installiert werden, potenzielle Kandidaten finden sich in der Liste `git mergetool --tool-help`. (vimdiff ist i.d.R. installiert, das ist aber gewöhnungsbedürftig).

* Mit dem Tool der Wahl muss dann der Konflikt gelöst werden. Beispielsweise: `git mergetool --tool=p4merge`

* Sofern das Tool das nicht macht, muss doch der manuelle Weg gewählt werden: Anpassen per `git add ...; git commit...`

* Die Dateien mit dem Suffix `.orig` müssen dann ggf. noch entsorgt werden.

Falls irrtümlich ein _merge_ begonnen wurde, kann dieser auch abgebrochen werden mit `git merge --abort`.


### Rebase

Um in den aktiven Branch die Änderungen aus einem anderen Branch zu integrieren und einen linearen Verlauf zu erhalten, gibt es eine zweite Variante der Zusammenführung: _Rebase_:

Die Änderungen des zweiten Branchs werden in die lineare Struktur des aktiven Branchs integriert, dazu werden neue Commits erstellt, die die bisherigen Commits des aktiven Branchs und des zweiten Branches vereinigen. Die Commits des zweiten Branches bleiben unverändert vorhanden.

Wir ergänzen beispielhaft im Quelltext eine Funktion zur Zeitansage:

```python
import datetime

def begruessung(name:str):
    print("Hallo", name, "!")

def tschuess(name:str):
    print("Auf Wiedersehen,", name, "!")

def saytime():
    print("Es ist "+str(datetime.datetime.now().strftime('%H:%M:%S')))

name = input("Bitte geben Sie den Namen ein: ")
begruessung(name)
saytime()
tschuess(name)
```

```bash
$ git checkout -b feature/time
$ git add -A ; git commit --message "feat: add timeinfo"
```
```
[feature/time eb2b14b] feat: add timeinfo
 1 file changed, 6 insertions(+)
```

Es wurden weitere Änderungen am Text vorgenommen ("Uhr" wurde ergänzt usw.). Das Ergebnis wird ebenso im Branch _feature/time_ angefügt.

```bash
 $ git add -A ; git commit --message "feat: correct typos"
 ```

 ```
 [feature/time 3a847cd] feat: correct typos
  1 file changed, 1 insertion(+), 1 deletion(-)
```

#### Anpassungen in _main_ vornehmen

Unterdessen wurde ein Tippfehler in _main_ entdeckt: es soll nach dem Vornamen, nicht nach dem Namen gefragt werden. Die Änderung soll direkt im Branch _main_ erfolgen. Dazu muss dieser zunächst wieder geladen werden (Achtung: die Dateien um Dateisystem ändern sich mit diesem Befehl):

```bash
$ git checkout main
```

```
Switched to branch 'main'
```

Änderung in der Python-Datei vornehmen, z.B. diese Zeile anpassen:

```python
name = input("Bitte geben Sie den Vornamen ein: ")
```

```bash
$ git add -A ; git commit --message "fix: ask politely for surname instead name"
```

```
[main 6f9bb56] fix: ask politely for surname instead name
 1 file changed, 1 insertion(+), 1 deletion(-)
```

Noch eine weitere Änderung in der Datei anpassen, damit der git-Graph interessanter wird, z.B. die Zeile in `hallo.py` ändern zu:

```python
name = input("Bitte seien Sie so freundlich und geben Sie ihren Vornamen ein: ")
```

Danach die gespeicherte Datei über die Stage in Repository übernehmen.

```bash
$ git add -A ; git commit --message "fix: ask very politely for surname instead name"
```

```bash
$ git log --all --decorate --oneline --graph
```

![Änderungen in main und feature/time als Vorbereitung für einen Rebase](images/graph-before-rebase.png)

<!--
Für http://bit-booster.com/graph.html
git log --all --date-order --pretty="%h|%p|%d %s"
3a9a2e5|6f9bb56| E (HEAD -> main) fix: ask very politely for surname instead name
6f9bb56|2ba714d| D fix: ask politely for surname instead name
3a847cd|eb2b14b| C (feature/time): feat: correct typos
eb2b14b|2ba714d| B : feat: add timeinfo
2ba714d|529594d 46a682f| A: feat: integrate two features
529594d|896a1da| (feature/Verabschiedung)
46a682f|fb45c39| (feature/Begruessung)
fb45c39|896a1da|
896a1da|7a910bf|
7a910bf|d04664e|
d04664e||

git log --all --decorate --oneline --graph
* 3a9a2e5 (HEAD -> main) fix: ask very politely for surname instead name
* 6f9bb56 fix: ask politely for surname instead name
| * 3a847cd (feature/time) feat: correct typos
| * eb2b14b feat: add timeinfo
|/
*   2ba714d feat: integrate two features
-->

Jetzt kommt das Zauberwerk: die Änderungen in C (also B und C) sollen auf den Änderungen in E (also D und E) ausgeführt werden. Dazu müssen wir zunächst in den `feature/time`-Branch wechseln und dort ein Verschieben der Änderungen (`rebase`) ausführen:


```bash
$ git checkout feature/time
$ git rebase main
```

Wenn jetzt alles automatisch vereinigt werden kann, dann ist alles wunderbar. Sollte es aber zu Konflikten kommen, so müssen diese wieder (wie bei `merge` auch) manuell oder mit Tools gelöst werden.
```
Auto-merging hallo.py
CONFLICT (content): Merge conflict in hallo.py
error: could not apply eb2b14b... feat: add timeinfo
hint: Resolve all conflicts manually, mark them as resolved with
hint: "git add/rm <conflicted_files>", then run "git rebase --continue".
hint: You can instead skip this commit: run "git rebase --skip".
hint: To abort and get back to the state before "git rebase", run "git rebase --abort".
Could not apply eb2b14b... feat: add timeinfo
```

Analog zu _merge_ können hier nicht alle Probleme automatisch gelöst werden, `hallo.py` macht Probleme.  Wir öffnen die Datei im Editor: die beiden Varianten sind wie oben beim _merge_- Konflikt zwischen `<<<<<<<`, `=======` und `>>>>>>>` gekennzeichnet.

```diff
...
<<<<<<< HEAD
def saytime():
    print("Es ist jetzt "+str(datetime.datetime.now().strftime('%H:%M:%S'))+" Uhr.")

name = input("Bitte geben Sie den Namen ein: ")
=======
name = input("Bitte geben Sie ihren Vornamen ein: ")
>>>>>>> ...
```
Im Editor löschen wir die überflüssigen Zeilen und Markierungen, speichern die Datei und melden Sie auf der Stage an:

```bash
$ git add hallo.py
$ git rebase --continue
```

Ein Editor öffnet sich für die Commit-Nachricht. Wenn der Editor geschlossen ist, wird der _Rebase_ abgeschlossen und es erscheint folgende Nachricht:

```
[detached HEAD 2741736] feat: add timeinfo
 1 file changed, 8 insertions(+), 1 deletion(-)
Successfully rebased and updated refs/heads/feature/time.
```

Hätte es beim Rebasen keine Konflikte gegeben, dann wäre in etwa eine solche Nachricht erschienen:

```
First, rewinding head to replay your work on top of it...
Fast-forwarded main to feature/time.
```

Das Ergebnis ist schon gar nicht schlecht, aber eben noch nicht fertig:

```bash
$ git log --all --decorate --oneline --graph
```

<!--
Für http://bit-booster.com/graph.html
git log --all --date-order --pretty="%h|%p|%d %s"
20f901f|2741736| C' (HEAD -> feature/time) feat: correct typos
2741736|3a9a2e5| B' feat: add timeinfo
3a9a2e5|6f9bb56| E ( main) fix: ask very politely for surname instead name
6f9bb56|2ba714d| D : fix: ask politely for surname instead name
3a847cd|eb2b14b| C (feature/time): feat: correct typos
eb2b14b|2ba714d| B : feat: add timeinfo
2ba714d|529594d 46a682f| A: feat: integrate two features
529594d|896a1da| (feature/Verabschiedung)
46a682f|fb45c39| (feature/Begruessung)
fb45c39|896a1da|
896a1da|7a910bf|
7a910bf|d04664e|
d04664e||


git log --all --decorate --oneline --graph
* 20f901f (HEAD -> feature/time) feat: correct typos
* 2741736 feat: add timeinfo
* 3a9a2e5 (main) ask very politely for surname instead name
* 6f9bb56 fix: ask politely for surname instead name
*   2ba714d feat: integrate two features
-->

![Der git-Graph nach dem Rebase: die feature-Commits sind dem main angehängt, die beiden feature/time-commits werden mit git log eigentlich nicht mehr angezeigt](images/graph-after-rebase.png)

Die Änderungen der Commits B und C aus dem `feature/time`-Branch wurden auf den letzten Commit von `main` angewendet. Die dadurch entstehenden neuen Commits wurden hier oben als `B'` und `C'` genannt, da sie ja die gleichen Änderungen wie `B` und `C` enthalten. `C'` ist der aktuelle Commit im Workspace, `B'` und weitere Vorgänger von `C'` aus dem `feature/time`-Branch haben bisher niemals auf irgendeinem System existiert - sind also ggf. nicht lauffähig, weil dieser Commit auch nie getestet wurde. Das ist eine Besonderheit bei der Nutzung von `rebase`. Die beiden vorigen Commits `B` und `C` werden von _git_ nicht mehr angezeigt und aus dem Commit-Graphen entfernt:

![Der git-Graph nach dem Rebase: die feature-Commits sind dem main angehängt, die beiden feature/time-commits werden mit git log eigentlich nicht mehr angezeigt](images/graph-after-rebase2.png)

Wenn man genau hinsieht, erkennt man, das bisher `main` noch auf den Commit `E` zeigt. Wir müssen das mit einem _fast forward merge_ noch ändern. Wir wählen `main` aus:

```bash
$ git checkout main
```

```
Switched to branch 'main'
```

 und _mergen_ `main` mit `feature/time`:

```bash
$ git merge feature/time
```

```
Updating 3a9a2e5..20f901f
Fast-forward
 hallo.py | 9 ++++++++-
 1 file changed, 8 insertions(+), 1 deletion(-)
```

Im Ergebnis erhalten wir eine wirklich lineare Historie:

![Der git-Graph nach dem Rebase: der main wurde wieder ans Ende gestellt per fast forward](images/graph-after-rebase3.png)

<!--
git log --all --date-order --pretty="%h|%p|%d %s"
20f901f|2741736| C' (HEAD -> main, feature/time) feat: correct typos
2741736|3a9a2e5| B' feat: add timeinfo
3a9a2e5|6f9bb56| E fix : ask very politely for surname instead name
6f9bb56|2ba714d| D : fix: ask politely for surname instead name
2ba714d|529594d 46a682f| A: feat: integrate two features

git log --all --decorate --oneline --graph
* 20f901f (HEAD -> main, feature/time) feat: correct typos
* 2741736 feat: add timeinfo
* 3a9a2e5 ask very politely for surname instead name
* 6f9bb56 fix: ask politely for surname instead name
*   2ba714d feat: integrate two features
-->

> Beim Rebasen werden die Änderungen des aktuellen Branches (im Beispiel: `feature/time`) an den als Argument genannten Branch (im Beispiel `main`) angehängt. Wir bleiben dabei im aktuellen Branch (`feature/time`). Der als Argument genannte Branch zeigt weiterhin auf den letzten Commit dieses Branches und muss ggf. noch mit einem Fast-Forward-Merge nachgezogen werden.
>

### Gezielt einzelne Commits als aktuell auswählen

In der Regel wird jeder neue Commit an das Ende des jeweiligen Branches gehängt. Man nennt den Zeiger auf den jeweils aktuellen Commit den `HEAD`. Analog zum Auswählen eines aktuellen Branches mit `git checkout branchname` (was `HEAD` auf den jeweils aktuellsten Commit des Branches setzt) können wir auch `HEAD` auf einzelne Commits setzen, in dem wir diese über deren Hashcode ansprechen.

Wo befindet sich aktuell der `HEAD`? `git log` verrät es uns (hier mit gekürzten Hashes):

```bash
$ git log --oneline
```

```
20f901f (HEAD -> main, feature/time) feat: correct typos
2741736 feat: add timeinfo
3a9a2e5 ask very politely for surname instead name
6f9bb56 fix: ask politely for surname instead name
2ba714d feat: integrate two features
```

`HEAD` hängt also derzeit am `main` und würde mit ihm wandern.

Den `HEAD` umsetzen auf einen anderen Commit setzen erfolgt über den (ggf. gekürzten) Hashwert:

```bash
$ git checkout 3a9a2e5
```

```
Note: switching to '3a9a2e5'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by switching back to a branch.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -c with the switch command. Example:

  git switch -c <new-branch-name>

Or undo this operation with:

  git switch -

Turn off this advice by setting config variable advice.detachedHead to false

HEAD is now at 3a9a2e5 ask very politely for surname instead name
```

Git warnt explizit, dass jetzt die Dinge etwas anders funktionieren...

Die per `git log` sichtbare Historie hat sich stark verjüngt, außerdem zeigt `HEAD` nicht mehr auf den `main`, sondern ist nur dem Commit angehängt:

```bash
$ git log --oneline
```

```
3a9a2e5 (HEAD) ask very politely for surname instead name
6f9bb56 fix: ask politely for surname instead name
2ba714d feat: integrate two features
```

Keine Angst, die anderen Commits sind nicht weg. Mit der Option `--all` zeigt `git log` die auch wieder an.

Der `HEAD` kann jederzeit (und sollte) wieder auf das Ende eines Branches gelegt werden.

```bash
$ git checkout main
```

```
Previous HEAD position was 3a9a2e5 ask very politely for surname instead name
Switched to branch 'main'
```

`HEAD` zeigt damit wieder auf den `main`

```bash
$ git log --oneline
```

```
20f901f (HEAD -> main, feature/time) feat: correct typos
2741736 feat: add timeinfo
3a9a2e5 ask very politely for surname instead name
6f9bb56 fix: ask politely for surname instead name
2ba714d feat: integrate two features
```

### Navigation im commit-tree

In _git_ kann man sich im Versionsbaum relativ und absolut bewegen. Mit dem Caret-Zeichen ^ (Zirkumflex) wird der direkte Vorgänger adressiert:

```bash
$ git checkout HEAD^
```

Für eine bestimmte Anzahl, die der Commit-Tree Richtung Wurzel durchschritten werden soll, kann die Tilde kombiniert mit einer Zahl angegeben werden. Drei Schritte zurück also z.B. mit:

```bash
$ git checkout HEAD~3
```

#### Branches über relative Referenzen bewegen
Sofern ein Branch verschoben werden soll, kann dies erfolgen über:

```bash
$ git branch -f branchname HEAD^
```

#### Änderungen rückgängig machen
Um lokal den Vorgänger wieder herzustellen und den Branch auf den Vorgänger zu legen tippen wir ein:

```bash
$ git reset HEAD~1
```

Sofern aber andere bereits mit dem so gelöschten Commit arbeiten ist dieser nicht aus der Historie verschwunden und wird ggf. später wieder referenziert. Besser geeignet ist hier die Methode, mit einem weiteren Commit die Änderungen des/der vorigen Commits ungeschehen zu machen:

```bash
$ git revert HEAD
```

### Cherry Picking

Um Änderungen aus einem anderen branch zu übernehmen, kann man gezielt eine Reihe von Commits auswählen, und deren Änderungen in den aktuellen Branch übernehmen. Hierfür ist natürlich wichtig, dass in den Commits sauber gearbeitet wurden und die Commit-Messages aussagekräftig sind (und alles beschreiben, was der Commit macht), damit nicht irgendwelche unvorhergesehenen Seiteneffekte auftreten.

```bash
$ git cherry-pick commit1 commit2
```

### Fazit

Zugegeben: Branches sind ein relativ knackiger Teil der _git_-Welt, gerade als Anfänger*in verrennt man sich da häufig etwas. Hinzu kommt, dass jedes Unternehmen seinen eigenen Workflow hat - und es somit keine einheitliche Aufteilung der Branches gibt. Wer aber den Unterschied zwischen `merge` und `rebase` schonmal verstanden hat ist gut gewappnet, auch die weiteren Irrungen und Wirrungen zu meistern, die die Versionsverwaltung bereithält.

### Teile dieses Tutorials

* [Ein git-Repository anlegen und lokal commiten](https://oer-informatik.de/git01-repo_anlegen_und_lokal_commiten)

* [Versionieren mit Branches](https://oer-informatik.de/git02-versionieren_mit_branches)

* [Git Remote nutzen](https://oer-informatik.de/git03-remote_nutzen)

* [Übungsaufgaben zu _git_](https://oer-informatik.de/git_uebungsaufgaben)

* [Readme.md Dateien für _git_-Repositorys erstellen](https://oer-informatik.de/git04-readme-erstellen)


### Weitere Literatur zur Versionsverwaltung `git`

- Zentrale Seite für Übungen zum Branching: [Learn git branching](https://learngitbranching.js.org/?locale=de_DE): animiertes Tutorial v.a. gut, um die Navigation im git-Tree zu verstehen.

- Wie würde _git_ klingen, wenn es Musik wäre? [re:bass von Dylan Beattie](https://www.yewtu.be/watch?v=S9Do2p4PwtE)

- **als Primärquelle**: die git-Dokumentation - lässt sich im Terminal mit ``git --help`` aufrufen oder unter [https://git-scm.com/doc](https://git-scm.com/doc)

- Als Info v.a. zu zentralen Workflows mit git: René Preißel / Bjørn Stachmann: Git (dPunkt Verlag, ISBN Print: 978-3-86490-649-7

- Aus der "von Kopf bis Fuß"-Serie von O'Reilly - erfrischend anders: "Head First Git" von Raju Gandhi (englisch), ISBN: 9781492092513

- Zahllose Tutorials im Internet wie z.B. dieses von [Atlassian](https://www.atlassian.com/de/git/tutorials/learn-git-with-bitbucket-cloud)
