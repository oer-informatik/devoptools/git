## _git_ im Team und Remote nutzen

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110326723919640390</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/git03-remote_nutzen</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Zugegeben: die meisten Leute meinen, wenn sie von git reden eher Plattformen wie GitHub, GitLab oder Bitbucket. Wir sollten diese zentralen Repositories als schleunigst einbinden... Neben diesem Teil gibt es noch die beiden Tutorialfolgen [Ein git-Repository anlegen und lokal commiten](https://oer-informatik.de/git01-repo_anlegen_und_lokal_commiten) und [Versionieren mit Branches](https://oer-informatik.de/git02-versionieren_mit_branches)._

### Ein lokales Repository erstellen

Zunächst wird ein lokales Repository benötigt, das später mit einem zentralen Repository verknüpft wird. Auf der obersten Ebene des Projektverzeichnisses erzeugen und aktualisieren wir das mit:

```bash
$ git init
$ git status      # nochmal kontrollieren: wird alles einbezogen, was einbezogen werden soll? Und ignoriert?
$ git add .
$ git commit -m "Initialisierung des Projekts"
```

### Neues zentrales Repository erstellen

Ein zentrale Repository kann über die üblichen Anbieter (z.B. _github_, _bitbucket_, _gitlab_) erstellt werden oder in Form eines _bare_-Repositorys im Filesystem auf einem Server gespeichert werden. Individuelle Anleitungen hierzu finden sich bei den jeweiligen Anbietern. Hierbei ist die Reihenfolge wichtig: soll ein vorhandenes lokales Repository übernommen werden, so ist wichtig, dass entsprechend ein leeres Remote-Repository erstellt wird.

### Zentrales Repository lokal als remote einstellen

Verknüpft wird das remote-Repository über den `remote`-Befehl und eine URL mit Protokollangabe:

```bash
$ git remote add origin https://github.com/testuser/testrepo.git
```

In diesem Beispiel wurde `https` als Protokoll verwendet. Es ist genauso möglich, per `SSH` oder `file` auf ein entferntes Repository zuzugreifen. Gegebenenfalls sind Firewall-Einstellungen und Portfreigaben zu beachten! Konkret: in unserem Schulnetz ist beispielsweise nur der Weg über HTTPS geöffnet, da die Standard-SSH-Ports blockiert werden.

### Eine weitere eigene Arbeitskopie eines entfernten Repository kopieren

Wenn ein Repository geklont wird, merkt sich das lokale Repository, woher es stammt - das Abbild des entfernten (remote) Repositorys wird _origin_ genannt. Jedes Teammitglied kopiert das Repository per `clone` auf die lokale Maschine:

```bash
$ git clone https://github.com/testuser/testrepo.git
```
```
On branch main
nothing to commit, working tree clean
```

Ein Branch des `origin` kann lokal über den Pfad `origin/main` angesprochen werden. Allerdings verhält es sich anders, als ein lokales Repository.

### Den aktuellen Stand eines remote-Repositorys auf das lokale übertragen

Die Änderungen des entfernten Repositorys wird mit dem Befehl `fetch` in das lokale Repository geladen. Es aktualisieren sich also alle Branches unter `origin`.

```bash
$ git fetch
```

Wenn sich das lokale Repository geändert hat, müssen die Änderungen des `remote` Repositorys noch übernommen werden - mit den Techniken, mit denen Änderungen aus anderen Branches übertragen werden  - also je nach Fall: `merge`, `rebase` oder `cherry-pick`.

```bash
$ git merge origin/main
```

Das Aktualisieren des `remote`-Repositorys und Übernehmen der Änderungen in den aktuellen Branch kann auch in einem Befehl (`pull`) erfolgen. Wenn dies per `rebase` stattfinden soll wäre das: `pull --rebase`.

```bash
$ git pull
```

![Die git-Befehle add, commit, push, fetch und pull im UML-Sequenzdiagramm](plantuml/04-git_remote.png)


### Ein Remote-Repositorys klonen und einem neuen Remote-Repository zuweisen:

Ein Remote-Repository klonen:

```bash
$ git clone https://gitlab.com/beispiel/beispielrepo.git
```

ggf. das Repository umbenennen
```bash
$ mv beispielrepo neuesrepo
```
in das Verzeichnis wechseln:

```bash
$ cd neuesrepo
```

bisherige _remote_-Repositories ausgeben:

```bash
$ git remote -v
```
```
origin  https://gitlab.com/beispiel/beispielrepo.git (fetch)
origin  https://gitlab.com/beispiel/beispielrepo.git (push)
```

Variante 1: Das alte Remote-Repository soll weiterhin zugreifbar bleiben:

```bash
$ git remote rename origin old-origin
$ git remote add origin https://gitlab.com/neuesRepo/neuesRepo.git
$ git push -u origin --all
$ git push -u origin --tags
```

Variante 2: Das alte Remote-Repository soll ersetzt werden:

```bash
$ git remote set-url origin https://gitlab.com/neuesRepo/neuesRepo.git
```
Überprüfen, ob das geklappt hat mit
```bash
$ git remote -v
```

<span class="hidden-text">

**Den aktuellen Stand eines lokalen-Repositorys auf das `remote`-Repository übertragen**

Der umgekehrte Weg: wir wollen unsere lokalen Änderungen auf das `remote` Repository übertragen. Hierzu muss zunächst sichergestellt sein, dass wir unsere Änderungen von der aktuellen Version des `remote`-Repositorys ableiten (s.o.: `pull`)

```bash
$ git push
```

TODO to be continued

</span>


### Ein Projekt aufsetzen: zu ignorierende Verzeichnisse vorbereiten

Bevor ein Projekt in die Versionsverwaltung übernommen werden kann, muss geplant werden, welche Dateien und Verzeichnisse in die Versionierung aufgenommen werden sollen.

Leere Verzeichnisse werden nicht von _git_ versioniert. Sofern sie dennoch einbezogen werden müssen (weil z.B. die IDE die Verzeichnisse benötigt), muss darin eine Datei erzeugt werden.

_git_ prüft in jedem Verzeichnis, ob es eine Datei `.gitignore` gibt, in der alle Dateien/Verzeichnisse gelistet sind, die nicht versioniert werden sollen (auch über _glob-character_ wie `*`). Ausnahmen werden dort mit vorangestelltem `!` notiert. Für häufig auszuschließende Dateitypen finden sich im Internet zahlreiche `.gitignore`-Vorlagen.

Beispiel: Im Verzeichnis `leeresVerzeichnis` müssten wir eine Datei `.gitignore` mit folgendem Inhalt aufnehmen:

```
#Alle Dateien bis auf diese Datei im aktuellen Verzeichnis ignorieren:
*	         # alle Dateien im Verzeichnis ignorieren
!.gitignore	 # bis auf diese Datei
```

Analog können ganze Verzeichnisse oder Datentypen ausgenommen werden. Um im Gesamtprojekt alle `*.class` Dateien (Java Bytecode) und das Verzeichnis `/tmp` nicht zu versionieren, erstellen wir im Projektverzeichnis eine Datei `.gitignore` mit dem Inhalt:

```
#In allen Unterverzeichnissen zu ignorierende Verzeichnisse und Dateitypen:
/tmp
*.class
```

### Umgang mit Zeilenenden festlegen

Die Betriebssysteme speichern die Zeilenenden in unterschiedlicher Weise: _carriage return_ (`CR` / `\r`) oder _line feed_  (`LF` / `\n`). Während Windows beide nutzt (`CR LF`) wird in unix-artigen Betriebssystemen nur `LF` verwendet. Um bei _cross-plattform_ Entwicklerteams nicht jeden Zeilenumbruch als Änderung ausgegeben zu bekommen, bietet sich folgendes Verfahren an:

Windows-Systeme: Ins Repository werden Unix-Zeilenenden geschrieben (`LF`), die für den Workspace betriebssystem-üblich umgewandelt werden:

```bash
$ git config --global core.autocrlf true
```

Unix/Linux/OSX-Systeme: Ins Repository werden Unix-Zeilenenden geschrieben (lf) und diese auch lokal genutzt:
```bash
$ git config --global core.autocrlf input
```

Selten kommt es vor, dass _git_ versucht, auch bei Binärdateien Zeilenenden anzupassen. Das würde natürlich zu Problemen führen. In der Datei `.gitattributes` können wir festlegen, welche Dateiendungen als Binärdatei behandelt werden (und nicht konvertiert werden) und welche als Text. Der Dateiinhalt könnte etwa so aussehen:

```
*.xml text
*.pdf binary
```

### Git hinter einem Proxy

Sofern das lokale Netzwerk einen Proxy verwendet muss sichergestellt werden, dass _git_ diesen verwendet. Bereits eingerichtete System-Proxys werden nicht immer erkannt. Hier hilft es den Proxy direkt in der _git config_ einzurichten. Beispielsweise würde ein Proxy, der ohne User-Authentifizierung auf Port `8080` der IP `10.1.1.3` lauscht, wie folgt in _git_ eingetragen (kein Schreibfehler: der Pfad wird in beiden Fällen mit `http://` angegeben, nicht `https://`):

```bash
$ git config --global http.proxy http://:@10.1.1.3:8080
$ git config --global https.proxy http://:@10.1.1.3:8080
```

Allgemein ist die URL wie folgt aufgebaut:
`http://proxyUsername:proxyPassword@proxy.server.com:port`

Wer in unterschiedlichen Netzen arbeitet (mal mit, mal ohne Proxy) muss die Einstellung auch wieder rückgängig machen. Den Proxy entfernt man aus der _git_-Konfiguration mit:

```bash
$ git config --global --unset http.proxy
$ git config --global --unset https.proxy
```

Alternativ kann der Proxy auch - nur für eine Session - über Umgebungsvariablen gesetzt werden:

```bash
$ set http_proxy=http://username:password@proxydomain:port
$ set https_proxy=http://username:password@proxydomain:port
$ set no_proxy=localhost,.my.company
```

Sofern es Probleme mit der Authentifizierung gibt, kann helfen, die Authentifizierung (innerhalb einer Domäne) über Windows umzusetzen:
```bash
$ git config --global credential.helper wincred
```

<!--*Falls keine gültigen ssl-Zertifikate vorliegen kann man diese Prüfung auch deaktivieren. Hiermit werden allerdings Sicherheitsvorkehrungen umgangen, daher sollte das bestenfalls genutzt werden, um zu prüfen, wo der Authentifizierungsfehler liegt.

```bash
$ git config http.sslVerify false
```

Nach Beheben des Fehlers sollte das in jedem Fall wieder aktiviert werden!-->

Um das lokale Repository erstmals auf das remote Repository zu übertragen, geben Sie ein (hier Beispiel mit `main`, bei älteren Installationen wird der Branch oft noch `master` genannt):

```bash
$ git push --set-upstream origin main
```

Damit wird der lokale `main`-Branch mit dem `main`-Branch des `origin/main` (lokale Kopie des `remote`-Repos) verknüpft.

### Umbenennen alter Bezeichnungen für den Haupt-Branch (Remote `master` in `main`)

Im Zuge der "Black Lives Matter"-Bewegung wurde auch die IT-Branche sprachsensibler. In diesem Zusammenhang wurde nach kultursensibleren Ausdrücken für Terminologien wie _master_/_slave_ gesucht. So auch bei `git`: Der Standard-Branch hieß vorher in den meisten `git`-Projekten `master`-Branch. Für neu angelegte Repositories wurde das in den akutellen `git`-Repositories geändert: dort heißt er nun (wie in diesem Tutorial durchgängig): `main`-Branch. 

Auch `git`-Dienste wie gitlab, github, bitbucket haben inzwischen nachgezogen und nutzen die neuen Bezeichnungen.^[Siehe z.B. [diesen Artikel über  GitHub](https://www.theserverside.com/feature/Why-GitHub-renamed-its-master-branch-to-main)]

Für ältere `git` gibt es jedoch keine automatische Umwandlung: Die Branch-Bezeichnung zieht sich durch zu viele Bereiche der Tools und Pipelines durch, Konfigurationsprobleme wären wahrscheinlich. Daher müssen ältere Projekte händisch angepasst werden. Ob das jeweilige Projekt betroffen ist, bekommt man am besten per `git status` heraus:

```bash
$ git status
```

```
On branch master
```

Für Repositories, die nur lokal vorliegen, ist die Umbenennung schnell gemacht: Wir nutzen die `move`-Option. Die Hilfe per `git branch -h` sagt uns dazu: _move/rename a branch and its reflog_. Genau das wollen wir:

```bash
$ git branch -m master main
```

Um dem lokalen _git_ jetzt mitzuteilen, dass beim _pushen_ der `main`-Branch des `remote`-Repositories verwendet werden soll, muss der Upstream-Branch mit der Option `-u` bzw. `--set-upstream` neu gesetzt werden:

```bash
git push -u origin main
```

Jetzt wissen eigentlich alle Bescheid. Dem jeweils genutzten Repository-Anbieter muss noch mitgeteilt werden, dass fortan `main` der Standard-Branch ist. Bei GitLab geht das z.B. unter _Settings / Repository / Default Branch_. Sofern weitere Tools (z.B. CI-Pipelines) genutzt werden, muss dort ebenfalls der Branch geändert werden (für  gitlab z.B.die `.gitlab-ci.yml`). Wenn das alles geschehen ist, kann der Upstream des `master`-Branches gelöscht werden:


```bash
git push origin --delete master
```

### Fazit

Wer mit _git branches_ umgehen kann hat wird auch mit den beiden neuen Befehlen `pull` und `push` keine Probleme bekommen. Gelegentlich ist das einbinden vorhandener Repositories etwas hakelig - beispielsweise, wenn sowohl lokal als auch remot ein Repo erstellt wurde, die nun zusammengeführt werden müssen. Im Zweifel hilft lokal nur der schmutzigste aller git-Tricks: das Verzeichnis `.git` löschen (und damit alle vorigen Versionen - also bitte wirklich nur direkt am Anfagn machen). Mit diesem Handwerkszeug sollte das Arbeiten in verteilten Teams jetzt kein Problem mehr sein.

Und falls doch Probelme auftauchen hilft neben datensparsamen Suchmaschinen auch immer ein einfaches `git help` in der Konsole.

### Teile dieses Tutorials

* [Ein git-Repository anlegen und lokal commiten](https://oer-informatik.de/git01-repo_anlegen_und_lokal_commiten)

* [Versionieren mit Branches](https://oer-informatik.de/git02-versionieren_mit_branches)

* [Git Remote nutzen](https://oer-informatik.de/git03-remote_nutzen)

* [Übungsaufgaben zu _git_](https://oer-informatik.de/git_uebungsaufgaben)

* [Readme.md Dateien für _git_-Repositorys erstellen](https://oer-informatik.de/git04-readme-erstellen)


### Weitere Literatur zur Versionsverwaltung `git`

- **als Primärquelle**: die git-Dokumentation - lässt sich im Terminal mit ``git --help`` aufrufen oder unter [https://git-scm.com/doc](https://git-scm.com/doc)

- Als Info v.a. zu zentralen Workflows mit git: René Preißel / Bjørn Stachmann: Git (dPunkt Verlag, ISBN Print: 978-3-86490-649-7

- Aus der "von Kopf bis Fuß"-Serie von O'Reilly - erfrischend anders: "Head First Git" von Raju Gandhi (englisch), ISBN: 9781492092513

- Zahllose Tutorials im Internet wie z.B. dieses von [Atlassian](https://www.atlassian.com/de/git/tutorials/learn-git-with-bitbucket-cloud)
