## Versionskontrollsystem _git_: Erste Schritte

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110326723919640390</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/git01-repo_anlegen_und_lokal_commiten</span>

> **tl/dr;** _(ca. 16 min Lesezeit): Bevor wir uns in die weite Welt von Github, Gitlab, Bitbucket usw. aufmachen, wollen wir erst einmal verstehen, was lokal passiert und ein paar Begriffe klären: Was ist ein Commit? Was ist ein Repository? Was ist der Workspace? Was ist die Stage? In welchen Schritten versioniere ich? Wie gestalte ich meine Commit-Nachrichten? Neben diesem Teil gibt es noch die beiden Tutorialfolgen [Versionieren mit Branches](https://oer-informatik.de/git02-versionieren_mit_branches) und [Git Remote nutzen](https://oer-informatik.de/git03-remote_nutzen)._

Bei _git_ handelt es sich um ein dezentrales Versionskontrollsystem. Es dient dazu, verschiedene Versionsstände eines Projektes zu verwalten. Häufig wird _git_ mit Diensten wie _Github_, _Gitlab_ oder _Bitbucket_ gleichgesetzt, denn _git_ übernimmt bei allen drei Diensten die Versionierung im Hintergrund. _git_ bleibt ein dezentraler Dienst, auch wenn das Projekt auf einer zentralen Website gehostet wird, denn jeder Client verfügt i.d.R. über alle vergangenen Versionsstände (die gesamte Historie).

Oft wird _git_ über Plugins oder über Integration in einer Entwicklungsumgebung genutzt. Wir wollen jedoch direkt die _git_-Befehle in der Konsole absetzen, um den Versionierungsprozess zu verstehen und um bei Versionierungsproblemen eingreifen zu können.

### _git_ Installieren

Die aktuelle _git_-Version lässt sich über die URL [https://git-scm.com/download](https://git-scm.com/download) laden.
Bei der Installation können im Wesentlichen Voreinstellungen verwendet werden, allerdings sollte folgendes beachtet werden:

+ Es sollte ein Editor nach Wahl eingestellt werden, andernfalls ist der für Einsteiger etwas sperrige _vim_ ausgewählt (_atom_, _notepad++_, _VSCode_: ganz nach Belieben).

+ _git_, aber nicht die Unix-Befehle von der Kommandozeile nutzen (voreingestellt)

+	Unix-Konvertierung der Zeilenumbrüche aktivieren (oberste Option)
Dann kann es losgehen. Starten Sie die _git-bash_!

### _git-bash_ starten und konfigurieren

Funktioniert _git_? In der Konsole kann die Versionsnummer abgefragt werden:

```bash
$ git --version
```

> git version 2.35.1.windows.2

Damit die einzelnen Versionen zum einen zuordenbar sind, zum anderen die Hash-Werte berechnet werden können, ist noch eine Personalisierung erforderlich, beispielsweise über diese beiden Befehle:

```bash
$ git config --global user.name martin.mustermann
$ git config --global user.email "martin.mustermann@test.de"
```

Damit ist alles einsatzbereit für die ersten Gehversuche mit _git_.

Welche Konfiguration aktuell eingestellt ist lässt sich überprüfen mit

```bash
$ git config --list
```

### Ein erstes Repository anlegen:

Die nötigen Dateien im .git Ordner lassen wir im Folgenden anlegen:
Navigieren Sie in der _git-bash_ mit `cd` in den Ort, an dem Sie ein Projektverzeichnis eröffnen wollen und erstellen dort mit `mkdir` ein Projektverzeichnis (ich nenne mein Verzeichnis "mein-git-versuch"):

```bash
$ mkdir mein-git-versuch
```

Navigieren Sie mit `cd` ins soeben erstellte Projektverzeichnis:

```bash
$ cd mein-git-versuch/
```

Erzeugen Sie für diesen Projektordner mit `git init` ein Repository. Was geschieht dadurch? _git_ legt einen Unterordner `.git` an (je nach Einstellung des Betriebssystems ist dieser versteckt, lässt sich aber in der _git-bash_ mit `ls -la` anzeigen). In diesem Unterordner speichert _git_ alle Versionsstände, und Konfigurationen dieses Repositories.

Mit der Option `-b main` kann man dafür sorgen, dass der Haupt-Zweig dieses neuen Versionierungsprojekts in jedem Fall "main" heißt (alte Git-Versionen nutzen noch den Namen "master", das ist heute aber unüblich):

```bash
$ git init -b main
```

Die Antwort des Systems:

```
Initialized empty Git repository in ../mein-git-versuch/
```

Den aktuellen Status des Repositorys kann man sich mit `git status` ausgeben lassen:

```bash
$ git status
```

```
On branch main
No commits yet
nothing to commit (create/copy files and use "git add" to track)
```

### Commit: Eine erste Projektversion erstellen

_git_ unterscheidet drei Stadien, in denen sich Dateien in einem Projekt befinden können:

* _Workspace_: Im Workspace sind alle Dateien, die aktuell im Dateisystem des Projektordners liegen.

* _Stage_: Dateien, die angemeldet sind, um der nächsten Version anzugehören, wurden quasi auf die Bühne (_stage_)  geholt. Sie können aber noch verändert werden, die aktuellen Änderungen sind noch nicht das Versionsverwaltungssystem eingefügt.

* _Repository_: Das Repository bezeichnet die Gesamtheit aller bisherigen Änderungen. Alle Versionsstände sind im Repository gespeichert. Im Dateisystem befinden sich diese Daten im `.git`-Unterordner. Wenn man eine neue Version in das Repostory übernimmt, spricht man von einem _commit_.

Bislang gibt es natürlich noch keine Datei zu versionieren. Wir werden jetzt eine als einfaches Beispiel im Projektverzeichnis eine neue Datei erstellen, _stagen_ und _commiten_. Wir erstellen also eine Python-Datei mit dem folgenden Inhalt und speichern sie als `hello.py` im Projektordner ab.

```python
name = input("Bitte den Namen eingeben: ")
print("Hallo", name, "!")
```

Die Datei liegt nun im Workspace. Eine erneute Ausgabe des Status' zeigt, dass _git_ zwar registriert hat, dass es eine neue Datei gibt, diese ist jedoch noch nicht zur Versionierung angemeldet ("untracked").

```bash
$ git status
```

```
On branch main
No commits yet
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        hello.py

nothing added to commit but untracked files present (use "git add" to track)
```

_git_ gibt uns aber auch direkt einen Hinweis, was wir tun können: Die neue Datei muss auf die _Stage_ gehoben werden. Hierzu dient der Befehl `git add`:

```bash
$ git add hello.py
```

Bei vielen Änderungen ist es natürlich mühsam, jede Datei einzeln anzumelden. _git_ kann mithilfe der Option `--all` (Kurzform `-A`) auch einfach jede Datei des Projektverzeichnisses ("Workspace") zur Versionierung anmelden

Der neue Status lässt sich wieder abfragen:

```bash
$ git status
```

```
On branch main
No commits yet
Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   hello.py
```

Im Hintergrund passiert noch gar nicht so wahnsinnig viel. Die _git_ weiß jetzt nur, dass diese Datei im nächsten Schritt versioniert werden soll.

![Datei für die nächste Version vormerken mit git add (UML-Sequenzdiagramm)](plantuml/01-git_add.png)

Noch ist die Datei nur vorgemerkt. Die vorbereiteten Änderungen können jetzt in eine Version des Projekts übernommen werden, wobei für die Versionsübersicht (das Log) eine Nachricht angefügt werden sollte, im einfachsten Fall mit der Option `--message` (Kurzform: `-m`).

```bash
$ git commit -m "new feature: say 'Hello NAME'"
```

```
[main (root-commit) d04664e] new feature: say 'Hello NAME'
 1 file changed, 2 insertions(+)
 create mode 100644 hello.py
```

In der Ausgabe finden wir erste Informationen zu unserem _Commit_:

*	Die ersten Zeichen des _Hashes_ ("d04664e"). Der Hashwert tritt bei _git_ an die Stelle einer Versionsnummer, die aufgrund der dezentralen Struktur nicht fortlaufend erstellt werden kann. Der Hash wird aus Autorenname und Commit-Zeitpunkt ermittelt und ist mit an Sicherheit grenzender Wahrscheinlichkeit eindeutig.

* Ab der dritten Zeile finden sich Informationen zu den enthaltenen Dateien, wie Name und Zugriffsrechte (im Beispiel: "100644" - nach Linux Konvention)

Bei einem Commit wird die Liste aller Dateien auf der Stage geholt und für jede Datei die Änderungen ermittelt. Diese Änderungen werden dann ins Repository geschrieben:

![Einen neuen Versionsstand erstellen mit git commit (UML-Sequenzdiagramm)](plantuml/02-git_commit.png)

Den aktuellen Status der veröffentlichten Projektversion:

```bash
$ git status
```

Und siehe da: alle Änderungen wurden erfasst. Workspace und Repository sind auf dem gleichen Stand, die Stage ist leer.

```
On branch main
nothing to commit, working tree clean
```

### Informationen zur aktuellen Version erhalten.

Zum Commit selbst können wir uns mit `git log` genauere Informationen ausgeben lassen, am kompaktesten mit der Option `--oneline`.

```bash
$ git log --oneline
```

```
d04664e (HEAD -> main) new feature: say 'Hello NAME'
```

Den kompletten Hashwert der _Commits_zeigt `log` ohne Optionen an:

```bash
$ git log
```

```
commit d04664ee3bb278cec2021a54c2f17d5fe86d002a (HEAD -> main)
Author: Martin Mustermann <martin.mustermann@test.de>
Date:   Mon Feb 7 17:40:13 2022 +0100

    new feature: say 'Hello NAME'
```

Detaillierte Informationen zu den einzelnen betroffenen Dateien erhält man mit der Option `--stat`:

```bash
$ git log --stat
```

```
commit d04664ee3bb278cec2021a54c2f17d5fe86d002a (HEAD -> main)
Author: martin.mustermann <martin.mustermann@test.de>Date:   Mon Feb 7 17:40:13 2022 +0100
    new feature: say 'Hello python2.7'
 hello.py | 2 ++
 1 file changed, 2 insertions(+)
```

### Dateien ändern

Wir erstellen beispielhaft eine neue Version. Es gibt ein paar kleine Änderungen und Ergänzungen:

```python
name = input("Bitte geben Sie den Namen ein: ")
print("Hallo", name, "!")
print("Auf Wiedersehen!")
```

Nach dem Speichern zeigt ein `git status` direkt, was zu tun ist:

```
On branch main
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   hello.py

no changes added to commit (use "git add" and/or "git commit -a")
```

Also, wie bei der ersten Runde, zunächst die Datei auf die Stage hieven:

```bash
$ git add hello.py
```

... dann eine neue Version speichern:

```bash
$ git commit -m "new feature: say Good-bye"
```

```
[main 7a910bf] new feature: say Good-bye
 1 file changed, 2 insertions(+), 1 deletion(-)
```

Die Ausgabe von `git log --oneline` wird jetzt schon interessanter:

```
7a910bf (HEAD -> main) new feature: say Good-bye
d04664e new feature: say 'Hello NAME'
```

### Versionsunterschiede darstellen

Was unterscheidet die eine Version von der anderen? Diese Informationen erhalten wir mit dem _git_ Kommando `diff`. Wenn wir zwei Commits vergleichen wollen übergeben wir (mindestens) die ersten vier Zeichen der Hashwerte (erst alte Version, dann neue Version):

```bash
$ git diff 7a91 d046
```

Die Ausgabe erfolgt in der Konsole farbig. Es werden gelöschte Zeilen (erstes Zeichen ein `-`), angefügte Zeilen (`+`) ausgegeben. In der Zeile mit dem Doppelklammeraffen wird angegeben:

```
@@  -alte Zeilennummer,  Länge des ausgeschnittenen +neueZeilennummer, Länge der Einfügung @@
```

```
diff --git a/hello.py b/hello.py
index d6abd70..a6f9d07 100644
--- a/hello.py
+++ b/hello.py
@@ -1,3 +1,2 @@
-name = input("Bitte geben Sie den Namen ein: ")
+name = input("Bitte den Namen eingeben: ")
 print("Hallo", name, "!")
-print("Auf Wiedersehen!")
```

Wer es noch genauer benötigt, der dann sich auch das _Diff_ auf Wortebene ausgeben lassen mithilfe der Option `--word-diff`. Hier werden die jeweiligen Passagen in Klammern gekennzeichnet: `[-GELÖSCHTE TEXTE-]` und `{+EINGEFÜGTE TEXTE+}`.

```bash
$ git diff 7a91 d046 --word-diff
```

```
diff --git a/hello.py b/hello.py
index d6abd70..a6f9d07 100644
--- a/hello.py
+++ b/hello.py
@@ -1,3 +1,2 @@
name = input("Bitte[-geben Sie-] den Namen [-ein:-]{+eingeben:+} ")
print("Hallo", name, "!")
[-print("Auf Wiedersehen!")-]
```

### Dateien verschieben, löschen, anfügen

Wir erstellen eine neue Datei:

```bash
$ touch readme.md
```

... und ändern den Namen unseres Python-Quelltextes.

```bash
$ mv hello.py  hallo.py
```

Wieder zeigt `git status` unmittelbar an, dass eine gelöschte (`hello.py`) noch Bestandteil des Repositories ist, wohingegen zwei andere noch nicht erfasst wurden (`hallo.py`, `readme.md`). Wir ändern das in einem Aufwasch:

```bash
$ git add -A; git commit -m "docs: add readme"
```

Eine Übersicht der unterschiedlichen Stadien, in denen sich ein git-versioniertes Projekt befinden kann (mit den jeweiligen _git_-Befehlen) findet sich in folgendem UML-Zustandsdiagramm:

![Die unterschiedlichen Zustände eines mit git versionierten lokalen Projekts im UML-Zustandsdiagramm ](plantuml/03-git_state.png)

### Aussagekräftige _commit-messages_

Für die ersten Gehversuche reicht für einen Commit eine kurze einzeilige Nachricht an unser zukünftiges ich. Es sollte im Imperativ formuliert sein und sich auf das _"Was?"_ und _"Warum?"_ konzentrieren.

In der _git_-Dokumentation ist davon die Rede, dass die Commit-Message aus _Subject_, _Description_ und _Footer_ bestehen soll, jeweils durch eine Leerzeile getrennt.^[[Customizing-Git-Git-Configuration](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration)]].

Eine verbreitete Umsetzung dieser Dreiteilung ist beispielsweise die _angular style_-Konvention für Commit-Nachrichten.^[[Angular Commit Convention](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#commit)] Die erste Zeile hat demnach folgenden Aufbau:

```bnf
TYPE[(SCOPE)]: SUBJECT
```

Die Zeile ist begrenzt auf maximal 50 Zeichen und besteht aus den Komponenten

* `TYPE`  (z.B. `feat`, `fix`, `refactor`, `test`, `docs`) gibt an, in welchem Umfang sich das Projekt geändert hat

* `SCOPE` ist optional und wird eingeklammert (z.B. `gui`, `plugin`, `logging-modul`). Sofern die Veränderung nur eine bestimmte Schicht oder Komponente betrifft, wird das in Klammern notiert.

* Das `SUBJECT` beginnt mit einem Großbuchstaben und endet ohne Punkt. Es fasst zusammen _was_ geändert wurde.

Für kleine Projekt und häufige Commits sollte der Einzeiler reichen. Wer aber in größeren Projekten arbeitet und die komplette Tagesleistung in einen Commit fasst sollte sich nicht auf Einzeiler begrenzen: oftmals ist es wichtig, sich genauer auf das _"Was?"_ und _"Warum?"_ zu beziehen. Der _angular_-stype sieht folgende Komponenten vor:

```bnf
TYPE[(SCOPE)]: SUBJECT

[BODY]

[FOOTER]
```

* Auf die Betreffzeile folgt nach einer Leerzeile die genaue Beschreibung, was sich geändert hat und v.a.: _warum_!

* Auch dieser Text wird im Imperativ geschrieben und sollte (wie Code generell) nach 72 Zeichen umbrechen. Es gilt das richtige Maß zu finden. Nur relevante Informationen, aber nicht zu wenige ("So einfach wie möglich, aber nicht einfacher" A. Einstein)

* Unterhalb des Bodys kann im _footer_ noch beschrieben werden, welches Ticket/Issue durch den Commit gelöst wurde.

### Teile dieses Tutorials

* [Ein git-Repository anlegen und lokal commiten](https://oer-informatik.de/git01-repo_anlegen_und_lokal_commiten)

* [Versionieren mit Branches](https://oer-informatik.de/git02-versionieren_mit_branches)

* [Git Remote nutzen](https://oer-informatik.de/git03-remote_nutzen)

* [Übungsaufgaben zu _git_](https://oer-informatik.de/git_uebungsaufgaben)

* [Readme.md Dateien für _git_-Repositorys erstellen](https://oer-informatik.de/git04-readme-erstellen)

### Weitere Literatur zur Versionsverwaltung `git`

- **als Primärquelle**: die git-Dokumentation - lässt sich im Terminal mit ``git --help`` aufrufen oder unter [https://git-scm.com/doc](https://git-scm.com/doc)

- Als Info v.a. zu zentralen Workflows mit git: René Preißel / Bjørn Stachmann: Git (dPunkt Verlag, ISBN Print: 978-3-86490-649-7

- Aus der "von Kopf bis Fuß"-Serie von O'Reilly - erfrischend anders: "Head First Git" von Raju Gandhi (englisch), ISBN: 9781492092513

- Zahllose Tutorials im Internet wie z.B. dieses von [Atlassian](https://www.atlassian.com/de/git/tutorials/learn-git-with-bitbucket-cloud)
